$(document).ready(adnemInit);

var adnemC2A = 'Contactar',
	adnemOpen = false, // Unused :-/
	adnemCDivision = '',
	adnemSubmitted = false;

var adnemDivisions = new Array();

adnemDivisions['MX001'] = 'Pak Mail Nazas';
adnemDivisions['MX003'] = 'Pak Mail Polanco';
adnemDivisions['MX004'] = 'Pak Mail Industrial La Loma';
adnemDivisions['MX006'] = 'Pak Mail Hermanos Serdán';
adnemDivisions['MX007'] = 'Pak Mail San Jerónimo';
adnemDivisions['MX008'] = 'Pak Mail Roma';
adnemDivisions['MX009'] = 'Pak Mail Oriente';
adnemDivisions['MX010'] = 'Pak Mail Lomas Verdes';
adnemDivisions['MX012'] = 'Pak Mail Ejercito Nacional';
adnemDivisions['MX013'] = 'Pak Mail WTC';
adnemDivisions['MX014'] = 'Pak Mail Villacoapa';
adnemDivisions['MX017'] = 'Pak Mail Tlalpan';
adnemDivisions['MX018'] = 'Pak Mail Atenas';
adnemDivisions['MX021'] = 'Pak Mail Felix Cuevas';
adnemDivisions['MX022'] = 'Pak Mail Echegaray';
adnemDivisions['MX023'] = 'Pak Mail Bellas Artes';
adnemDivisions['MX024'] = 'Pak Mail Plaza Las Águilas';
adnemDivisions['MX026'] = 'Pak Mail Tlahuac';
adnemDivisions['MX027'] = 'Pak Mail 16 de Septiembre';
adnemDivisions['MX029'] = 'Pak Mail Palenque';
adnemDivisions['MX032'] = 'Pak Mail Bosques de la Herradura';
adnemDivisions['MX033'] = 'Pak Mail Coacalco';
adnemDivisions['MX036'] = 'Pak Mail San Juan del Rio';
adnemDivisions['MX038'] = 'Pak Mail Chamizal';
adnemDivisions['MX039'] = 'Pak Mail Metepec';
adnemDivisions['MX042'] = 'Pak Mail Veracruz Centro';
adnemDivisions['MX043'] = 'Pak Mail Ensenada';
adnemDivisions['MX044'] = 'Pak Mail Cozumel';
adnemDivisions['MX045'] = 'Pak Mail Miramontes';
adnemDivisions['MX047'] = 'Pak Mail Tepepan';
adnemDivisions['MX048'] = 'Pak Mail Reforma';
adnemDivisions['MX049'] = 'Pak Mail Playa del Carmen';
adnemDivisions['MX051'] = 'Pak Mail Plan de Ayala';
adnemDivisions['MX052'] = 'Pak Mail Lindavista';
adnemDivisions['MX053'] = 'Pak Mail Los Reyes Coyoacán';
adnemDivisions['MX055'] = 'Pak Mail 31 Poniente';
adnemDivisions['MX056'] = 'Pak Mail Benjamin Franklin';
adnemDivisions['MX057'] = 'Pak Mail Plutarco Elias Calles';
adnemDivisions['MX059'] = 'Pak Mail San Mateo';
adnemDivisions['MX061'] = 'Pak Mail Izcalli';
adnemDivisions['MX064'] = 'Pak Mail Mixcoac';
adnemDivisions['MX068'] = 'Pak Mail Antonio Caso';
adnemDivisions['MX072'] = 'Pak Mail Ecatepec';
adnemDivisions['MX073'] = 'Pak Mail Xalostoc';
adnemDivisions['MX075'] = 'Pak Mail Saltillo';
adnemDivisions['MX076'] = 'Pak Mail Av. Juárez';
adnemDivisions['MX077'] = 'Pak Mail Plaza Real';
adnemDivisions['MX079'] = 'Pak Mail Parque Lincoln';
adnemDivisions['MX080'] = 'Pak Mail Tecamachalco';
adnemDivisions['MX081'] = 'Pak Mail Valle Dorado';
adnemDivisions['MX083'] = 'Pak Mail Cuitláhuac';
adnemDivisions['MX087'] = 'Pak Mail Plaza Cuajimalpa';
adnemDivisions['MX090'] = 'Pak Mail Mérida';
adnemDivisions['MX093'] = 'Pak Mail 1ro de Mayo';
adnemDivisions['MX094'] = 'Pak Mail Av. Toluca';
adnemDivisions['MX095'] = 'Pak Mail Bosques del Lago';
adnemDivisions['MX096'] = 'Pak Mail Cumbres';
adnemDivisions['MX098'] = 'Pak Mail Venustiano Carranza Sur';
adnemDivisions['MX099'] = 'Pak Mail Centro Cd. de México';
adnemDivisions['MX100'] = 'Pak Mail Torre Pemex';
adnemDivisions['MX102'] = 'Pak Mail Obrero Mundial';
adnemDivisions['MX103'] = 'Pak Mail Virreyes';
adnemDivisions['MX107'] = 'Pak Mail Cuernavaca Centro';
adnemDivisions['MX108'] = 'Pak Mail Plaza Bosques';
adnemDivisions['MX110'] = 'Pak Mail Donceles';
adnemDivisions['MX111'] = 'Pak Mail Bolívar';
adnemDivisions['MX114'] = 'Pak Mail Valladolid';
adnemDivisions['MX115'] = 'Pak Mail Plaza Polanco';
adnemDivisions['MX116'] = 'Pak Mail Del Valle';
adnemDivisions['MX118'] = 'Pak Mail Taxqueña';
adnemDivisions['MX119'] = 'Pak Mail Universidad';
adnemDivisions['MX120'] = 'Pak Mail Cuautla Centro';
adnemDivisions['MX121'] = 'Pak Mail Peñon de los Baños';
adnemDivisions['MX123'] = 'Pak Mail Toluca 2000';
adnemDivisions['MX124'] = 'Pak Mail Portales';
adnemDivisions['MX126'] = 'Pak Mail Quinta Real';
adnemDivisions['MX127'] = 'Pak Mail La Vía Cuautitlán';
adnemDivisions['MX130'] = 'Pak Mail Plaza Gran Patio';
adnemDivisions['MX132'] = 'Pak Mail Veracruz Av. España';
adnemDivisions['MX133'] = 'Pak Mail Querétaro Centro';
adnemDivisions['MX137'] = 'Pak Mail San Miguel Chapultepec';
adnemDivisions['MX138'] = 'Pak Mail Viveros Coyoacán';
adnemDivisions['MX140'] = 'Pak Mail Orizaba Av Circunvalación';
adnemDivisions['MX141'] = 'Pak Mail Palmas';
adnemDivisions['MX142'] = 'Pak Mail Plaza Churubusco';
adnemDivisions['MX146'] = 'Pak Mail Monterrey Lomas del Valle';
adnemDivisions['MX147'] = 'Pak Mail Lomas de Sotelo';
adnemDivisions['MX150'] = 'Pak Mail Ermita';
adnemDivisions['MX151'] = 'Pak Mail Del Carmen Coyoacan';
adnemDivisions['MX153'] = 'Pak Mail Xalapa Av. Antonio Chedraui';
adnemDivisions['MX154'] = 'Pak Mail Pericoapa';
adnemDivisions['MX155'] = 'Pak Mail Granjas México';
adnemDivisions['MX156'] = 'Pak Mail Satélite';
adnemDivisions['MX158'] = 'Pak Mail Lomas Estrella';
adnemDivisions['MX160'] = 'Pak Mail Américas';
adnemDivisions['MX161'] = 'Pak Mail Plaza del Sol';
adnemDivisions['MX165'] = 'Pak Mail Morelos';
adnemDivisions['MX166'] = 'Pak Mail Plaza Fontana';
adnemDivisions['MX167'] = 'Pak mail León 1';
adnemDivisions['MX168'] = 'Pak Mail Vallejo';
adnemDivisions['MX169'] = 'Pak Mail Aguascalientes';
adnemDivisions['MX170'] = 'Pak Mail Plaza Pocitos';
adnemDivisions['MX171'] = 'Pak Mail Culiacán';
adnemDivisions['MX175'] = 'Pak Mail Mazatlan';
adnemDivisions['MX176'] = 'Pak Mail Riviera Nayarita';
adnemDivisions['MX178'] = 'Pak Mail Tlalnepantla Centro';
adnemDivisions['MX179'] = 'Pak Mail San Pedro de los Pinos';
adnemDivisions['MX180'] = 'Pak Mail Hacienda Ojo de Agua';
adnemDivisions['MX182'] = 'Pak Mail Espacio Esmeralda';
adnemDivisions['MX183'] = 'Pak Mail La Diana';
adnemDivisions['MX184'] = 'Pak Mail San Mateo Atenco';
adnemDivisions['MX188'] = 'Pak Mail Coyuya';
adnemDivisions['MX189'] = 'Pak Mail Plaza Tanarah';
adnemDivisions['MX190'] = 'Pak Mail Diagonal San Antonio Narvarte';
adnemDivisions['MX191'] = 'Pak Mail Mariscal Sucre Del Valle';
adnemDivisions['MX192'] = 'Pak Mail San Bernabé';
adnemDivisions['MX195'] = 'Pak Mail Concentro Zapopan';
adnemDivisions['MX196'] = 'Pak Mail Luis Cabrera';
adnemDivisions['MX197'] = 'Pak Mail Centro Interlomas';
adnemDivisions['MX198'] = 'Pak Mail Valle Oriente';
adnemDivisions['MX199'] = 'Pak Mail 1ra Sur Playa del Carmen';
adnemDivisions['MX201'] = 'Pak Mail La Paz';
adnemDivisions['MX202'] = 'Pak Mail Lomas de Angelópolis';
adnemDivisions['MX204'] = 'Pak Mail Acatlan';
adnemDivisions['MX205'] = 'Pak Mail Lomas de la Victoria';
adnemDivisions['MX206'] = 'Pak Mail Valle Poniente';
adnemDivisions['MX207'] = 'Pak Mail Tijuana';
adnemDivisions['MX208'] = 'Pak Mail Los Mochis';
adnemDivisions['MX209'] = 'Pak Mail Contreras';
adnemDivisions['MX210'] = 'Pak Mail Las Torres';
adnemDivisions['MX211'] = 'Pak Mail Manzanillo';
adnemDivisions['MX212'] = 'Pak Mail Palo Solo';
adnemDivisions['MX213'] = 'Pak Mail Infiniti';
adnemDivisions['MX214'] = 'Pak Mail Girasoles';
adnemDivisions['MX215'] = 'Pak Mail Circuito Metropolitan';
adnemDivisions['MX216'] = 'Pak Mail  Deportiva Villahermosa';
adnemDivisions['MX217'] = 'Pak Mail Plaza Norte';
adnemDivisions['MX218'] = 'Pak Mail Mariano Otero';
adnemDivisions['MX225'] = 'Pak Mail Coatzacoalcos';
adnemDivisions['MX227'] = 'Pak Mail Plaza La Venta';
adnemDivisions['MX230'] = 'Pak Mail Pennsylvania';
adnemDivisions['MX231'] = 'Pak Mail Zaragoza';

function adnemInit()
{
	adnemLayout();
	
	$('#adnem .mi').on('focus blur',fifh);
}

function fifh(e)
{
	var o = $(e.target),
		v = $(o).val(),
		l = $(o).siblings('label'),
		s = emptyString(v);
	
	switch(e.type)
	{
		case 'focus':
			$(l).addClass('aa');
		break;
		case 'blur':
			if(s)
			{
				$(l).removeClass('aa');
			}
		break;
	}
}

function emptyString(v){return (v.length === 0 || !v.trim());}

function cc(e)
{
	adnemCDivision = adnemDivisions[e];

	$('#adnem form .dd').html('Ingresa tus datos para recibir atención de ' + adnemCDivision + '.');

	adnemSet();
}

function adnemLayout()
{
	var adnemHTML = `
<div id="adnem">
	<div class="wrapper">
		<div class="shade">
			<div class="container">
				<div class="content">
					<div class="cc"><i class="material-icons">clear</i></div>
					<form method="post" autocomplete="off">
						<span class="dt">Formulario de contacto</span>
						<span class="dd"></span>
						<fieldset>
							<label for="nombre">Nombre</label>
							<input tabindex="1" class="mi" type="text" name="nombre">
						</fieldset>
						<fieldset>
							<label for="apellidos">Apellidos</label>
							<input tabindex="2" class="mi" type="text" name="apellidos">
						</fieldset>
						<fieldset>
							<label for="telefono">Teléfono</label>
							<input tabindex="3" class="mi" type="number" name="telefono">
						</fieldset>
						<fieldset>
							<label for="email">E-Mail</label>
							<input tabindex="4" class="mi" type="email" name="email">
						</fieldset>
						<fieldset>
							<label for="mensaje">Mensaje</label>
							<textarea tabindex="5" class="mi" name="mensaje"></textarea>
						</fieldset>
						<fieldset>
							<div class="g-recaptcha" data-sitekey="6LfZ7C4UAAAAAMoV6k2yN6d7i2NQwMPomf1OtHrp"></div>
						</fieldset>
						<fieldset>
							<div class="notification">Llena los campos y da clic en "Contactar"</div>
						</fieldset>
						<fieldset>
							<input tabindex="7" type="submit" name="" value="Contactar">
						</fieldset>
						<span class="dl"><a href="https://www.seccionamarilla.com.mx/aviso-privacidad-usuarios" target="_blank">Conoce cómo protegemos tus datos personales <i class="material-icons">launch</i></a></span>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>`;
	
	$('body').prepend(adnemHTML);
	
	adnemListenersSet();
	
	adnemLoadScripts();
}

function adnemLoadScripts()
{
	var adnemCSS = '';
		adnemCSS += '<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600" rel="stylesheet">';
		adnemCSS += '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">';
		adnemCSS += '<style type="text/css">#adnem{z-index:9999999;}#adnem,#adnem .wrapper,#adnem .wrapper .shade{width:100%;height:100%}:focus{outline:0}#adnem{display:none;position:fixed;left:0;top:0;font-family:\'Source Sans Pro\',sans-serif}::-webkit-input-placeholder{font-family:\'Source Sans Pro\',sans-serif;font-size:12px;color:#9fa3a8;text-transform:none}::-moz-placeholder{font-family:\'Source Sans Pro\',sans-serif;font-size:12px;color:#9fa3a8;text-transform:none}:-ms-input-placeholder{font-family:\'Source Sans Pro\',sans-serif;font-size:12px;color:#9fa3a8;text-transform:none}:-moz-placeholder{font-family:\'Source Sans Pro\',sans-serif;font-size:12px;color:#9fa3a8;text-transform:none}.invalid::-webkit-input-placeholder{color:#9e1414;text-transform:none}.invalid::-moz-placeholder{color:#9e1414;text-transform:none}.invalid:-ms-input-placeholder{color:#9e1414;text-transform:none}.invalid:-moz-placeholder{color:#9e1414;text-transform:none}#adnem .scd{transform:scale(0);opacity:0}#adnem .scb{transform:scale(1.1)}#adnem .wrapper .shade{position:relative;background-color:rgba(0,0,0,.6)}#adnem .wrapper .shade .container{width:50%;max-width:450px;min-width:320px;position:absolute;left:20%;top:20%;background-color:#eee;border:1px solid #bbb;border-radius:3px;opacity:0;-webkit-box-shadow:-4px 4px 5px 0 rgba(50,50,50,.3);-moz-box-shadow:-4px 4px 5px 0 rgba(50,50,50,.3);box-shadow:-4px 4px 5px 0 rgba(50,50,50,.3)}#adnem .wrapper .shade .container .content{width:96%;padding:11px 2%;position:relative}#adnem .wrapper .shade .container .content .cc{position:absolute;right:3px;top:3px;cursor:pointer}#adnem .wrapper .shade .container .content .cc i{font-size:14px;color:#636363}#adnem .wrapper .shade .container .content form{width:100%}#adnem .wrapper .shade .container .content form span{display:block;width:100%;font-family:\'Source Sans Pro\',sans-serif;font-weight:400;color:#636363}#adnem .wrapper .shade .container .content form span.dt{padding-top:10px;font-size:16px;font-weight:700}#adnem .wrapper .shade .container .content form span.dd{padding:0 0 10px;font-size:13px}#adnem .wrapper .shade .container .content form span.dl,#adnem .wrapper .shade .container .content form span.dl i{padding:8px 0;text-align:center;font-size:12px}#adnem .wrapper .shade .container .content form span.dl a,#adnem .wrapper .shade .container .content form span.dl a:hover{text-decoration:underline;color:#4286f4}#adnem .wrapper .shade .container .content form fieldset .invalid{border-bottom:solid 1px #9e1414!important}#adnem .wrapper .shade .container .content form fieldset{width:100%;margin:10px 0 0;padding:5px 0;position:relative;border:none}#adnem .wrapper .shade .container .content form fieldset .notification{display:block;width:96%;padding:6px 2%;font-size:12px;text-align:center;color:#636363;transition:all .2s ease-out}#adnem .wrapper .shade .container .content form fieldset .notification i{font-size:12px;color:#636363}#adnem .wrapper .shade .container .content form fieldset .g-recaptcha{min-height:82px;text-align:center;transition:transform .1s ease-out}#adnem .wrapper .shade .container .content form fieldset .g-recaptcha>div{display:inline-block}#adnem .wrapper .shade .container .content form fieldset label{position:absolute;left:2%;top:8px;pointer-events:none;font-size:13px;color:#636363;transition:all .2s ease-out}#adnem .wrapper .shade .container .content form fieldset label.aa{top:-5px;left:0;color:#878787;font-size:10px}#adnem .wrapper .shade .container .content form fieldset input[type=text],#adnem .wrapper .shade .container .content form fieldset input[type=number],#adnem .wrapper .shade .container .content form fieldset input[type=email],#adnem .wrapper .shade .container .content form fieldset textarea{width:calc(96% - 2px);margin:0;padding:3px 2%;font-size:13px;-webkit-appearance:none;background:0 0;color:#636363;border:none;border-bottom:solid 1px rgba(12,86,206,.3)}#adnem .wrapper .shade .container .content form fieldset input[name=nombre],#adnem .wrapper .shade .container .content form fieldset input[name=apellidos]{text-transform:capitalize}#adnem .wrapper .shade .container .content form fieldset input[name=email]{text-transform:lowercase}#adnem .wrapper .shade .container .content form fieldset input[type=submit]{width:100%;-webkit-appearance:none;background-color:#0c56ce;border:none;border-radius:3px;font-family:\'Source Sans Pro\',sans-serif;font-size:18px;font-weight:400;color:#fff;cursor:pointer;padding:4px 0;-webkit-box-shadow:-4px 4px 5px 0 rgba(50,50,50,.3);-moz-box-shadow:-4px 4px 5px 0 rgba(50,50,50,.3);box-shadow:-4px 4px 5px 0 rgba(50,50,50,.3);transition:all .2s ease-out}#adnem .wrapper .shade .container .content form fieldset :disabled{opacity:.6}#adnem .wrapper .shade .container .content form fieldset input[type=submit]:hover{background-color:#186bf2;transform:scale(1.013)}#adnem .loading-request{background-position:center center;background-repeat:no-repeat;background-size:auto 90%;background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwcHgiICBoZWlnaHQ9IjIwMHB4IiAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiIGNsYXNzPSJsZHMtZmFjZWJvb2siIHN0eWxlPSJiYWNrZ3JvdW5kOiBub25lOyI+ICAgIDxyZWN0IG5nLWF0dHIteD0ie3tjb25maWcueDF9fSIgbmctYXR0ci15PSJ7e2NvbmZpZy55fX0iIG5nLWF0dHItd2lkdGg9Int7Y29uZmlnLndpZHRofX0iIG5nLWF0dHItaGVpZ2h0PSJ7e2NvbmZpZy5oZWlnaHR9fSIgbmctYXR0ci1maWxsPSJ7e2NvbmZpZy5jMX19IiB4PSIxNy41IiB5PSIzMCIgd2lkdGg9IjE1IiBoZWlnaHQ9IjQwIiBmaWxsPSIjZmZmZmZmIj4gICAgICA8YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJ5IiBjYWxjTW9kZT0ic3BsaW5lIiB2YWx1ZXM9IjE4OzMwOzMwIiBrZXlUaW1lcz0iMDswLjU7MSIgZHVyPSIxIiBrZXlTcGxpbmVzPSIwIDAuNSAwLjUgMTswIDAuNSAwLjUgMSIgYmVnaW49Ii0wLjJzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSI+PC9hbmltYXRlPiAgICAgIDxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9ImhlaWdodCIgY2FsY01vZGU9InNwbGluZSIgdmFsdWVzPSI2NDs0MDs0MCIga2V5VGltZXM9IjA7MC41OzEiIGR1cj0iMSIga2V5U3BsaW5lcz0iMCAwLjUgMC41IDE7MCAwLjUgMC41IDEiIGJlZ2luPSItMC4ycyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiPjwvYW5pbWF0ZT4gICAgPC9yZWN0PiAgICA8cmVjdCBuZy1hdHRyLXg9Int7Y29uZmlnLngyfX0iIG5nLWF0dHIteT0ie3tjb25maWcueX19IiBuZy1hdHRyLXdpZHRoPSJ7e2NvbmZpZy53aWR0aH19IiBuZy1hdHRyLWhlaWdodD0ie3tjb25maWcuaGVpZ2h0fX0iIG5nLWF0dHItZmlsbD0ie3tjb25maWcuYzJ9fSIgeD0iNDIuNSIgeT0iMjkuODM5NSIgd2lkdGg9IjE1IiBoZWlnaHQ9IjQwLjMyMSIgZmlsbD0iI2ZmZmZmZiI+ICAgICAgPGFuaW1hdGUgYXR0cmlidXRlTmFtZT0ieSIgY2FsY01vZGU9InNwbGluZSIgdmFsdWVzPSIyMC45OTk5OTk5OTk5OTk5OTY7MzA7MzAiIGtleVRpbWVzPSIwOzAuNTsxIiBkdXI9IjEiIGtleVNwbGluZXM9IjAgMC41IDAuNSAxOzAgMC41IDAuNSAxIiBiZWdpbj0iLTAuMXMiIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIj48L2FuaW1hdGU+ICAgICAgPGFuaW1hdGUgYXR0cmlidXRlTmFtZT0iaGVpZ2h0IiBjYWxjTW9kZT0ic3BsaW5lIiB2YWx1ZXM9IjU4LjAwMDAwMDAwMDAwMDAxOzQwOzQwIiBrZXlUaW1lcz0iMDswLjU7MSIgZHVyPSIxIiBrZXlTcGxpbmVzPSIwIDAuNSAwLjUgMTswIDAuNSAwLjUgMSIgYmVnaW49Ii0wLjFzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSI+PC9hbmltYXRlPiAgICA8L3JlY3Q+ICAgIDxyZWN0IG5nLWF0dHIteD0ie3tjb25maWcueDN9fSIgbmctYXR0ci15PSJ7e2NvbmZpZy55fX0iIG5nLWF0dHItd2lkdGg9Int7Y29uZmlnLndpZHRofX0iIG5nLWF0dHItaGVpZ2h0PSJ7e2NvbmZpZy5oZWlnaHR9fSIgbmctYXR0ci1maWxsPSJ7e2NvbmZpZy5jM319IiB4PSI2Ny41IiB5PSIyOS40ODU5IiB3aWR0aD0iMTUiIGhlaWdodD0iNDEuMDI4MyIgZmlsbD0iI2ZmZmZmZiI+ICAgICAgPGFuaW1hdGUgYXR0cmlidXRlTmFtZT0ieSIgY2FsY01vZGU9InNwbGluZSIgdmFsdWVzPSIyNDszMDszMCIga2V5VGltZXM9IjA7MC41OzEiIGR1cj0iMSIga2V5U3BsaW5lcz0iMCAwLjUgMC41IDE7MCAwLjUgMC41IDEiIGJlZ2luPSIwcyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiPjwvYW5pbWF0ZT4gICAgICA8YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJoZWlnaHQiIGNhbGNNb2RlPSJzcGxpbmUiIHZhbHVlcz0iNTI7NDA7NDAiIGtleVRpbWVzPSIwOzAuNTsxIiBkdXI9IjEiIGtleVNwbGluZXM9IjAgMC41IDAuNSAxOzAgMC41IDAuNSAxIiBiZWdpbj0iMHMiIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIj48L2FuaW1hdGU+ICAgIDwvcmVjdD4gIDwvc3ZnPg==)}</style>';
	
	$('head').append(adnemCSS);
	//$('head').append('<link rel="stylesheet" type="text/css" href="adnem.css" />');
	
	$.getScript("https://www.google.com/recaptcha/api.js");
}

function adnemListenersSet()
{
	//$(document).keypress(function(e){adnemSet();}); // Eventually comment this out.
	
	$('#adnem .content .cc').on('click',adnemDismiss);
	$('#adnem .content input[type="submit"]').on('click',adnemSubmit);
}

function adnemListenersUnset()
{
	var o = '';
	
	o += '#adnem .content .cc,';
	o += '#adnem .content input[type="submit"]';
	
	$(o).off();
}

function adnemSet()
{
	grecaptcha.reset();
	
	$('#adnem').fadeIn('fast',adnemPosition);

	var c = '#adnem .wrapper .shade .container',
		ww = $(window).width() * .5,
		cw = $(c).width() * .5,
		fl = ww - cw,
		props = {'left':fl};

	$(c).css(props);
}

function adnemPosition()
{
	var ww = $(window).width() * .5,
		wh = $(window).height() * .5,
		cw = $('#adnem .container').width() * .5,
		ch = $('#adnem .container').height() * .5,
		fl = ww - cw,
		ft = wh - ch;

		fl = fl > 0 ? fl:0;
		ft = ft > 0 ? ft:0;

	var c = '#adnem .wrapper .shade .container',
		props = {
					'left':fl,
					'top':ft,
					'opacity':1
				};

	$(c).animate(props);
	
	$(window).resize(adnemPosition);
}

function adnemSubmit(e)
{
	e.preventDefault();
	
	switch($(this).val())
	{
		case adnemC2A:
		case '':
			if(!adnemSubmitted)
			{
				if(adnemValidate())
				{
					adnemSubmitStatus('loading');
					
					adnemSubmitted = true;
					
					adnemFieldsEnabled(false);
					
					var no = '#adnem input[name="nombre"]',
						ao = '#adnem input[name="apellidos"]',
						to = '#adnem input[name="telefono"]',
						eo = '#adnem input[name="email"]',
						mo = '#adnem textarea[name="mensaje"]',
						nv = $(no).val(),
						av = $(ao).val(),
						tv = $(to).val(),
						ev = $(eo).val(),
						mv = $(mo).val();
					
					var o = {};
					o.nombre = nv;
					o.apellidos = av;
					o.telefono = tv;
					o.email = ev;
					o.mensaje = mv;
					o.division = adnemCDivision;

					adnemRequestSet(o);
					
					adnemNotify('Guardando información...');
				}
			}
			else
			{
				console.log('multiple submit disabled');
			}
		break;
		case 'Cerrar':
			adnemDismiss();
		break;
	}
	
	return false;
}


function adnemRequestSet(o)
{
	var oo = JSON.stringify(o);
	
	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "https://blog.seccionamarilla.com.mx/srv/index.php?c=notify&m=pmsc_sm",
		"method": "POST",
		"headers": {"content-type": "application/json"},
		"processData": false,
		"data": o}
	
	$.post("https://blog.seccionamarilla.com.mx/srv/index.php?c=notify&m=pmsc_sm",o).done(adnemRequestGet).fail(adnemRequestError);
}

function adnemRequestGet(response)
{
	if(response == 'processed')
	{
		adnemNotify('Información guardada. <i class="material-icons">done</i><br>En breve personal de ' + adnemCDivision + ' contactará contigo.');
		
		adnemCaptchaSet(false);
		
		adnemSubmitStatus('dismiss');
	}
}

function adnemRequestError(error)
{
	console.error(error);
}

function adnemSubmitStatus(s)
{
	var e = '#adnem .content input[type="submit"]';
	
	switch(s)
	{
		case 'normal':
			$(e).val(adnemC2A).removeClass('loading-request');
		break;
		case 'loading':
			$(e).val('').addClass('loading-request');
		break;
		case 'dismiss':
			$(e).val('Cerrar').removeClass('loading-request');
		break;
	}
}

function adnemFieldsEnabled(s)
{
	var e = '#adnem input[name="nombre"],';
		e += '#adnem input[name="apellidos"],';
		e += '#adnem input[name="telefono"],';
		e += '#adnem input[name="email"],';
		e += '#adnem textarea[name="mensaje"]';
	
	if(s)
	{
		$(e).removeAttr('disabled');
	}
	else
	{
		$(e).attr('disabled','disabled');
	}
}

function adnemValidate()
{
	var re = true,
		no = '#adnem input[name="nombre"]',
		ao = '#adnem input[name="apellidos"]',
		to = '#adnem input[name="telefono"]',
		eo = '#adnem input[name="email"]',
		nv = $(no).val(),
		av = $(ao).val(),
		tv = $(to).val(),
		ev = $(eo).val();
	
	if(!validStr(nv)){adnemValidationErrorSet(false,no,'Ingresa tu nombre');re = false;}
	else{adnemValidationErrorSet(true,no,'Nombre');}
	if(!validStr(av)){adnemValidationErrorSet(false,ao,'Ingresa tus apellidos');re = false;}
	else{adnemValidationErrorSet(true,ao,'Apellidos');}
	if(!validStr(tv)){adnemValidationErrorSet(false,to,'Ingresa tu número de teléfono');re = false;}
	else{adnemValidationErrorSet(true,to,'Teléfono');}
	if(!validEmail(ev)){adnemValidationErrorSet(false,eo,'Ingresa tu E-Mail');re = false;}
	else{adnemValidationErrorSet(true,eo,'E-Mail');}
	if(grecaptcha.getResponse().length == 0){re = false;adnemCaptchaValidationNotice();}
	
	return re;
}

function adnemValidationErrorSet(t,o,s)
{
	if(!t)
	{
		//$(o).val('').attr('placeholder',s).addClass('invalid');
		$(o).val('').addClass('invalid').trigger('blur');
	}
	else
	{
		//$(o).attr('placeholder',s).removeClass('invalid');
		$(o).removeClass('invalid').trigger('blur');
	}
}

function adnemCaptchaSet(s)
{
	var e = 'form fieldset .g-recaptcha';
	
	if(!s)
	{
		$(e).fadeOut('fast');
	}
	else
	{
		$(e).show();
	}
}

function adnemCaptchaValidationNotice()
{
	var o = '#adnem .g-recaptcha';
	
	$(o).addClass('scb');
	
	setTimeout(function()
	{
		$(o).removeClass('scb');
	},100);
}

function adnemDismiss()
{
	var c = '#adnem .wrapper .shade .container',
		props = {
//					'left':'20%',
					'top':'20%',
					'opacity':0
				};
	
	$(c).animate(props,'fast');

	$('#adnem').fadeOut('slow',adnemReset);
}

function adnemReset()
{
	var i = '#adnem .wrapper .shade .container .content form fieldset input[type="text"],';
		i += '#adnem .wrapper .shade .container .content form fieldset input[type="number"],';
		i += '#adnem .wrapper .shade .container .content form fieldset input[type="email"],';
		i += '#adnem .wrapper .shade .container .content form fieldset textarea';

	$(i).removeClass('invalid').val('');
	
	$('.aa').removeClass('aa');
	
	adnemSubmitStatus('normal');
	
	adnemFieldsEnabled(true);
	
	adnemCaptchaSet(true);
	
	grecaptcha.reset();
	
	adnemSubmitted = false;
}

function adnemNotify(str)
{
	$('#adnem .notification').addClass('scd');
	
	setTimeout(function()
	{
		$('#adnem .notification').html(str);
	},200);
	
	setTimeout(function()
	{
		$('#adnem .notification').removeClass('scd');
	},400);
}

function validStr(str){return str.length > 2 && str.trim() != '';}
function validEmail(e){var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;return re.test(e);}