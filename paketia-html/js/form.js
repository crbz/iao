  ;
  (function ($) {

    function CalculateWeight(parcels, weight_unit, public_quoting) {
      VOL_WEIGHT_CALC_FACTOR_CM = 5000;
      VOL_WEIGHT_CALC_FACTOR_IN = 138.4;
      this.parcels = parcels;
      this.weight_unit = weight_unit ? weight_unit : 'kg';
      this.dimension_unit = weight_unit == 'lbs' ? 'in' : 'lbs';
      this.public = public_quoting;
      this.vol_weight_calc_factor = weight_unit == 'lbs' ? VOL_WEIGHT_CALC_FACTOR_IN : VOL_WEIGHT_CALC_FACTOR_CM;
      this.init();
    };

    CalculateWeight.prototype = {

      constructor: CalculateWeight,

      init: function () {

      },

      volumetricWeight: function (parcel) {
        var that = this;
        if (parcel != undefined) {
          var index = parcel['index'];
          var weight_unit = parcel['weight_unit'];
          var vol_weight = that.calculateSingleVolWeightQuantity(parcel);
        }

        var vol_div = $("#vol-div-" + index);
        if (vol_weight > 0) {
          vol_div.show('fast');
          $("#vol-weight-" + index).html(parseFloat(vol_weight).toFixed(2) >= 0.01 ? parseFloat(vol_weight)
            .toFixed(2) : "0.01" + " " + weight_unit);
        } else {
          vol_div.hide('fast');
        }
      },

      completeWeight: function () {
        var that = this;

        var total_physical_weight = that.calculatePhysicalWeight();
        var total_volumetric_weight = that.calculateVolWeight();
        var total_invoice_weight = that.calculateTotalInvoicedWeight();

        var weight_div = $('#weight');
        if (total_invoice_weight > 0 || total_physical_weight > 0 || total_volumetric_weight > 0) {
          if (that.public) {
            weight_div.css('display', 'inline-flex');
          } else {
            weight_div.css('display', 'block');
          }
          $('#total-invoiced').html(parseFloat(total_invoice_weight).toFixed(2) + " " + that.weight_unit);
          $('#total-physical').html(parseFloat(total_physical_weight).toFixed(2) + " " + that.weight_unit);
          $('#total-volumetric').html((parseFloat(total_volumetric_weight).toFixed(2) >= 0.01 ? parseFloat(
            total_volumetric_weight).toFixed(2) : "0.01") + " " + that.weight_unit);

          $("[rel='tooltip']").tooltip();
        } else {
          weight_div.hide('fast');
        }
      },

      calculateVolWeight: function () {
        var that = this;
        var result = 0.0;

        this.parcels.forEach(function (parcel, index, array) {
          var w = that.calculateSingleVolWeightQuantity(parcel);
          result += w
        });
        return result
      },
      calculateTotalInvoicedWeight: function () {
        var result = 0.0;
        var that = this;

        this.parcels.forEach(function (parcel, index, array) {

          var single_vol = that.calculateSingleVolWeightQuantity(parcel, that.vol_weight_calc_factor);
          var single_physical = that.calculateSinglePhysicalWeight(parcel);

          if (single_physical > single_vol) {
            result += single_physical;
          } else {
            result += single_vol;
          }
        });
        return result

      },
      calculatePhysicalWeight: function () {
        var that = this;
        var result = 0.0;

        this.parcels.forEach(function (parcel, index, array) {
          w = that.calculateSinglePhysicalWeight(parcel);
          result += w
        });
        return result
      },
      calculateSinglePhysicalWeight: function (parcel) {
        if (parcel['weight'] && parcel['quantity']) {
          return parseFloat(parcel['weight']) * parseFloat(parcel['quantity'])
        } else {
          return 0
        }
      },

      calculateSingleVolWeightQuantity: function (parcel) {
        if (parcel['length'] && parcel['height'] && parcel['width'] && parcel['quantity']) {
          return (parseFloat(parcel['length']) * parseFloat(parcel['height']) * parseFloat(parcel['width']) *
            parseFloat(parcel['quantity'])) / this.vol_weight_calc_factor
        } else {
          return 0
        }
      },

      calculateSingleVolWeight: function (parcel) {
        if (parcel['length'] && parcel['height'] && parcel['width']) {
          return (parseFloat(parcel['length']) * parseFloat(parcel['height']) * parseFloat(parcel['width'])) /
            this.vol_weight_calc_factor
        } else {
          return 0
        }
      }
    };

    window.CalculateWeight = CalculateWeight;
  }(jQuery));





  $(function () {
    $('#doc-btn').on('click', function () {
      removeDisabledFromWeightUnit();
    });

    $('#paq-btn').on('click', function () {
      removeDisabledFromWeightUnit();
    });

    $('#quote_from').bind("keydown.autocomplete", function () {
      removeDisabledFromWeightUnit();
      if ($("input[name=insure_shipment]").is(":checked") && $("#quote_from").val().length > 2) {
        changeInsuranceCurrency();
      }
    });

    $("input[name=insure_shipment]").click(function () {
      if ($("input[name=insure_shipment]").is(":checked") && $("#quote_from").val().length > 2) {
        changeInsuranceCurrency();
      }
    });

    $('input[name=insure_shipment]').on('change', function () {
      if ($(this).is(':checked')) {
        $('#insurance-info-link').show('fast');
      } else {
        $('#insurance-info-link').hide('fast');
      }
    });

    $('#quote_to').bind("keydown.autocomplete", function () {
      removeDisabledFromWeightUnit();
    });

    $('.form-subtabs-container').on('click', function () {
      if ($('.remove-volume-data').length == 1) {
        $('.remove-volume-data').hide();
      } else {
        $('.remove-volume-data').show();
      }
    });

    var removeDisabledFromWeightUnit = function () {
      var origin_country = $('#from').find('.selected-flag').find('.iti-flag').attr('class').slice(9, 11);
      var destination_country = $('#to').find('.selected-flag').find('.iti-flag').attr('class').slice(9, 11);
      if (origin_country == "us") {
        $('select[name="weight_unit"]').attr('disabled', false);
        $('select[name="weight_unit"]').val("lbs");
        document.getElementById('choose_lbs').click();
      }
      if (origin_country == "us" || destination_country == "us") {
        $('.switch-measure-selection').show();
      }

      if (origin_country != destination_country) {
        $('select[name="insured_currency"]').attr('disabled', false);
      }

    };

    var changeInsuranceCurrency = function () {
      var origin_country = $('#from').find('.selected-flag').find('.iti-flag').attr('class').slice(9, 11);
      var destination_country = $('#to').find('.selected-flag').find('.iti-flag').attr('class').slice(9, 11);
      $.get("quote/change_insurance_currency", {
        'origin': origin_country,
        'destination': destination_country
      }).done(function (response) {
        $("select[name='insured_currency']").empty();
        if (response.length > 1) {
          $('#insurance-currency-label').hide();
          $("select[name='insured_currency']").show();
          $.each(response, function (index, currency) {
            $("select[name='insured_currency']").append('<option value="' + currency + '"> ' + currency +
              ' </option>');
          })
        } else if (response.length == 1) {
          $("select[name='insured_currency']").hide();
          $('#insurance-currency-label').show();
          $('#insurance-currency-label').text(response[0])
        }
      });
    };

    var countryCodeToLocale = function (country) {
      if (country == 'cl') {
        return "es-CL";
      } else if (country == 'us') {
        return "en-US";
      } else if (country == 'mx') {
        return "es-MX";
      } else if (country == 'de') {
        return "de-DE";
      }
      return "";
    };

    var set_locale_cookie = function (selection) {
      document.cookie = "locale=" + countryCodeToLocale(selection);
    };

    $('.bfh-countries').on('change.bfhselectbox', function () {
      var country = $(this).val().toLowerCase();
      if (country == 'cl') {
        if (document.location.host.substr(document.location.host.length - 2) == 'mx') {
          window.location.replace(document.location.origin.replace('com.mx', 'cl'));
        } else {
          set_locale_cookie(country);
        }
      } else {
        if (document.location.host.substr(document.location.host.length - 2) == 'cl') {
          window.location.replace(document.location.origin.replace('cl', 'com.mx'));
        } else {
          set_locale_cookie(country);
        }
      }
    });

    var getCookie = function (cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
      }
      return "";
    };

    var set_contact_info = function (country) {
      var $contact_info = $("<p>Insurgentes sur N° 3500,<br />Torre Telmex primer piso,<br />C.P. 14060 México, CDMX</p>");
     // var contact_phone = '800 202 7272';
        var contact_phone = $("<p><a href='tel:+018002027272'>800 202 7272</a></p>");
      if (country == 'cl') {
        $contact_info = $(
          "<p>Apoquindo 5400, 0f 202<br />Las Condes<br />RM<br />Santiago de Chile<br />Chile</p>");
        contact_phone = '+56998782649';
      }
      $('#contact_info').html($contact_info);
     // $('.contact-options .phone-span').html(contact_phone);
        $('#phone-span').html(contact_phone);
    };

    set_contact_info(document.location.host.substr(document.location.host.length - 2));

    var lookupCountryByIP = function (callback) {
      $.get("//freegeoip.net/json/", function () {}, "jsonp").always(function (response) {
        if (response.country_code) {
          callback(response.country_code.toLowerCase());
          set_contact_info(response.country_code.toLowerCase());
        } else {
          callback("mx");
        }
      });
    };

    var lookupCountryByCookieOrDomain = function (callback) {
      var country = getCookie('locale');
      if (country == '') {
        var length = document.domain.length;
        country = document.domain.substr(length - 2);
        set_locale_cookie(country);
      } else {
        country = country.substr(3);
      }
      country = country.toLowerCase();
      if (country == 'cl') {
        callback("cl");
        set_contact_info("cl");
      } else {
        callback("mx");
      }
    };

    $(".flag-input-from").intlTelInput({
      onlyCountries: ['ac', 'ad', 'ae', 'af', 'ag', 'ai', 'al', 'am', 'an', 'ao', 'aq', 'ar', 'as', 'at', 'au',
        'aw', 'ax', 'az', 'ba', 'bb', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bl', 'bm', 'bn', 'bo', 'bq',
        'br', 'bs', 'bt', 'bv', 'bw', 'by', 'bz', 'ca', 'cc', 'cd', 'cf', 'cg', 'ch', 'ci', 'ck', 'cl', 'cm',
        'cn', 'co', 'cp', 'cr', 'cu', 'cv', 'cw', 'cx', 'cy', 'cz', 'de', 'dg', 'dj', 'dk', 'dm', 'do', 'dz',
        'ea', 'ec', 'ee', 'eg', 'eh', 'er', 'es', 'es-cn', 'et', 'eu', 'fi', 'fj', 'fk', 'fm', 'fo', 'fr',
        'ga', 'gb', 'gd', 'ge', 'gf', 'gg', 'gh', 'gi', 'gl', 'gm', 'gn', 'gp', 'gq', 'gr', 'gs', 'gt', 'gu',
        'gw', 'gy', 'hk', 'hm', 'hn', 'hr', 'ht', 'hu', 'ic', 'id', 'ie', 'il', 'im', 'in', 'io', 'iq', 'ir',
        'is', 'it', 'je', 'jm', 'jo', 'jp', 'ke', 'kg', 'kh', 'ki', 'km', 'kn', 'kp', 'kr', 'kw', 'ky', 'kz',
        'la', 'lb', 'lc', 'li', 'lk', 'lr', 'ls', 'lt', 'lu', 'lv', 'ly', 'ma', 'mc', 'md', 'me', 'mf', 'mg',
        'mh', 'mk', 'ml', 'mm', 'mn', 'mo', 'mp', 'mq', 'mr', 'ms', 'mt', 'mu', 'mv', 'mw', 'mx', 'my', 'mz',
        'na', 'nc', 'ne', 'nf', 'ng', 'ni', 'nl', 'no', 'np', 'nr', 'nu', 'nz', 'om', 'pa', 'pe', 'pf', 'pg',
        'ph', 'pk', 'pl', 'pm', 'pn', 'pr', 'ps', 'pt', 'pw', 'py', 'qa', 'qo', 're', 'ro', 'rs', 'ru', 'rw',
        'sa', 'sb', 'sc', 'sd', 'se', 'sg', 'sh', 'si', 'sj', 'sk', 'sl', 'sm', 'sn', 'so', 'sr', 'ss', 'st',
        'sv', 'sx', 'sy', 'sz', 'ta', 'tc', 'td', 'tf', 'tg', 'th', 'tj', 'tk', 'tl', 'tm', 'tn', 'to', 'tr',
        'tt', 'tv', 'tw', 'tz', 'ua', 'ug', 'um', 'us', 'uy', 'uz', 'va', 'vc', 've', 'vg', 'vi', 'vn', 'vu',
        'wf', 'ws', 'ye', 'yt', 'za', 'zm', 'zw'
      ],
      initialCountry: "auto",
      preferredCountries: ['mx', 'cl', 'us'],
      fieldType: "from",
      geoIpLookup: lookupCountryByCookieOrDomain
    });

    $(".flag-input-to").intlTelInput({
      onlyCountries: ['ac', 'ad', 'ae', 'af', 'ag', 'ai', 'al', 'am', 'an', 'ao', 'aq', 'ar', 'as', 'at', 'au',
        'aw', 'ax', 'az', 'ba', 'bb', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bl', 'bm', 'bn', 'bo', 'bq',
        'br', 'bs', 'bt', 'bv', 'bw', 'by', 'bz', 'ca', 'cc', 'cd', 'cf', 'cg', 'ch', 'ci', 'ck', 'cl', 'cm',
        'cn', 'co', 'cp', 'cr', 'cu', 'cv', 'cw', 'cx', 'cy', 'cz', 'de', 'dg', 'dj', 'dk', 'dm', 'do', 'dz',
        'ea', 'ec', 'ee', 'eg', 'eh', 'er', 'es', 'es-cn', 'et', 'eu', 'fi', 'fj', 'fk', 'fm', 'fo', 'fr',
        'ga', 'gb', 'gd', 'ge', 'gf', 'gg', 'gh', 'gi', 'gl', 'gm', 'gn', 'gp', 'gq', 'gr', 'gs', 'gt', 'gu',
        'gw', 'gy', 'hk', 'hm', 'hn', 'hr', 'ht', 'hu', 'ic', 'id', 'ie', 'il', 'im', 'in', 'io', 'iq', 'ir',
        'is', 'it', 'je', 'jm', 'jo', 'jp', 'ke', 'kg', 'kh', 'ki', 'km', 'kn', 'kp', 'kr', 'kw', 'ky', 'kz',
        'la', 'lb', 'lc', 'li', 'lk', 'lr', 'ls', 'lt', 'lu', 'lv', 'ly', 'ma', 'mc', 'md', 'me', 'mf', 'mg',
        'mh', 'mk', 'ml', 'mm', 'mn', 'mo', 'mp', 'mq', 'mr', 'ms', 'mt', 'mu', 'mv', 'mw', 'mx', 'my', 'mz',
        'na', 'nc', 'ne', 'nf', 'ng', 'ni', 'nl', 'no', 'np', 'nr', 'nu', 'nz', 'om', 'pa', 'pe', 'pf', 'pg',
        'ph', 'pk', 'pl', 'pm', 'pn', 'pr', 'ps', 'pt', 'pw', 'py', 'qa', 'qo', 're', 'ro', 'rs', 'ru', 'rw',
        'sa', 'sb', 'sc', 'sd', 'se', 'sg', 'sh', 'si', 'sj', 'sk', 'sl', 'sm', 'sn', 'so', 'sr', 'ss', 'st',
        'sv', 'sx', 'sy', 'sz', 'ta', 'tc', 'td', 'tf', 'tg', 'th', 'tj', 'tk', 'tl', 'tm', 'tn', 'to', 'tr',
        'tt', 'tv', 'tw', 'tz', 'ua', 'ug', 'um', 'us', 'uy', 'uz', 'va', 'vc', 've', 'vg', 'vi', 'vn', 'vu',
        'wf', 'ws', 'ye', 'yt', 'za', 'zm', 'zw'
      ],
      initialCountry: "auto",
      preferredCountries: ['mx', 'cl', 'us', 'es'],
      fieldType: "to",
      geoIpLookup: lookupCountryByCookieOrDomain
    });

    var c_from = $('#country_from').val();
    var c_to = $('#country_to').val();

    var url = "https://enviaya.com.mx/shipping/address_short_search.json?q=QUERY";

    var addresses_source = function (location_id) {
      return new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
          url: url,
          replace: function (xhr, postalcode) {
            $(".flag-input-" + location_id).parents('.input-group').find('.fa-sync').addClass(
              'glyphicon-spin');
            xhr = xhr.replace('QUERY', postalcode);
            return xhr + "&country=" + $(".flag-input-" + location_id).intlTelInput(
              "getSelectedCountryData").iso2;
          },
          filter: function (parsedResponse) {
            $(".flag-input-" + location_id).parents('.input-group').find('.fa-sync').removeClass(
              'glyphicon-spin');
            return parsedResponse;
          }
        }
      });
    };

    $('#quote_from.typeahead').bind('typeahead:select', function (ev, suggestion) {
      $("#id_from").val(suggestion.id)
    });

    $('#quote_to.typeahead').bind('typeahead:select', function (ev, suggestion) {
      $("#id_to").val(suggestion.id)
    });

    var typeaheadCountry = function (location) {
      $('#quote_' + location + '.typeahead').typeahead({
        highlight: true,
        minLength: 3
      }, {
        name: 'quote_' + location,
        display: 'value',
        source: addresses_source(location),
        limit: 20,
        templates: {
          suggestion: function (data) {
            return '<span>' + data.value + '</span>';
          }
        }
      });
    };

    var setIntlTelInputPlaceholders = function () {
      var changeCountry = function ($input, country_code) {
        var country = (country_code === undefined) ? document.domain.substr(length - 2) : country_code
        if (country == "mx") {
          $input.prop("placeholder", "Ciudad o código postal.");
        } else if (country == "cl" || country == "co" || country == "ve") {
          $input.prop("placeholder", "Ingresa una ciudad");
          $('#origin-search-label').text('Origen (Ciudad)');
          $('#destination-search-label').text('Destino (Ciudad)');
        } else {
          $input.prop("placeholder", "Ingresa un código postal o una ciudad.");
        }
      };
      ["#quote_from", "#quote_to"].forEach(function (element) {
        $(element).on("countrychange", function (e, countryData) {
          changeCountry($(this), $(this).intlTelInput("getSelectedCountryData").iso2);
        });
        changeCountry($(element), $(element).intlTelInput("getSelectedCountryData").iso2);
      });
    };

    var calculateTotalWeight = function () {
      var packages = [];
      var weight_unit = $('#parcel-weight-unit').val();
      var public_quoting = true;
      $.each($.find('.volume-data'), function (index, element) {
        var package_div = $(element);


        var quantity_value = package_div.find('input[name="shipment[parcels][' + index + '][quantity]"]')
        .val();
        var length_value = package_div.find('input[name="shipment[parcels][' + index + '][length]"]').val();
        var width_value = package_div.find('input[name="shipment[parcels][' + index + '][width]"]').val();
        var height_value = package_div.find('input[name="shipment[parcels][' + index + '][height]"]').val();
        var weight_value = package_div.find('input[name="shipment[parcels][' + index + '][weight]"]').val();

        var package_hash = {
          "quantity": "1",
          "length": length_value,
          "width": width_value,
          "height": height_value,
          "weight": weight_value
        };
        packages.push(package_hash);
      });
      calc_weight = new CalculateWeight(packages, weight_unit, public_quoting);
      calc_weight.completeWeight();
    };
    $('#paquete').delegate(('.volume-data input'), 'change', function () {
      calculateTotalWeight();
    });

    $('form').on('keypress', '.js-integers', function (e) {
      var key = e.charCode ? e.charCode : e.keyCode;
      // 13 is the enter key
      return (key >= 48 && key <= 57) || key == 13;
    });

    $('form').on('keypress', '.js-separators', function (e) {
      var separator = 44;
      var key = e.charCode ? e.charCode : e.keyCode;
      if ($.inArray(getCookie('locale'), ['en-US']) == 0 || $.inArray(getCookie('locale'), ['es-MX']) == 0) {
        separator = 46;
      }
      // 13 is the enter key
      return key == separator || (key >= 48 && key <= 57) || key == 13;
    });

    // // set list of available carriers for each domain
    // var set_carrier_list = function() {
    //   var length = document.domain.length;
    //   country = document.domain.substr(length-2);
    //   country = country.toLowerCase();

    //   var add_carrier_to_list = function($carrier_item) {
    //     $('#carousel-example-generic').find('.list-unstyled').append($carrier_item);
    //   };

    //   $dhl_item = $('<li><a href="http://www.dhl.com.mx"><img src="img/dhl-logo-white.png" alt="DHL"></a></li>');
    //   $fedex_item = $('<li><a href="http://www.fedex.com/mx/"><img src="img/fedex-logo.png" alt="FedEx"></a></li>');
    //   $ups_item = $('<li><a href="https://www.ups.com/"><img src="img/ups-logo-white.png" alt="UPS"></a></li>');
    //   $estafeta_item = $('<li><a href="http://www.estafeta.com/"><img src="img/estafeta-logo.png" alt="Estafeta"></a></li>');
    //   $redpack_item = $('<li><a href="http://www.redpack.com.mx/"><img src="img/redpack-logo-white.png" alt="Redpack"></a></li>');

    //   if(country == 'cl') {
    //     add_carrier_to_list($dhl_item);
    //     add_carrier_to_list($fedex_item);
    //     add_carrier_to_list($ups_item);
    //   }
    //   else {
    //     add_carrier_to_list($dhl_item);
    //     add_carrier_to_list($ups_item);
    //     add_carrier_to_list($redpack_item);
    //   }
    // };

    $(document).ready(function () {

      typeaheadCountry('from');
      typeaheadCountry('to');

      // set_carrier_list();

      setIntlTelInputPlaceholders();

      if ($.inArray(getCookie('locale'), ['en-US']) == 0 || $.inArray(getCookie('locale'), ['es-MX']) == 0) {
        $('#quote-weight').val('0.5');
      } else {
        $('#quote-weight').val('0,5');
      };

      var locale = getCookie('locale');
      if (locale != '') {
        $('.bfh-countries').val(locale.substr(3).toUpperCase());
      }

      var match = RegExp('[?&]' + "gclid" + '=([^&]*)').exec(window.location.search);
      var gclid = match && decodeURIComponent(match[1].replace(/\+/g, ' '));
      if (gclid) {
        setGclidCookie('gclid', gclid, 90);
      }

      function setGclidCookie(name, value, days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        document.cookie = name + "=" + value + expires;
      }

      var showError = function (target, message) {
        var position = {
          at: 'bottom center',
          my: 'top left'
        };
        $(target).qtip({
          content: message,
          position: position,
          show: {
            event: 'load',
            ready: true
          },
          hide: {
            event: 'keypress click'
          },
          style: {
            classes: 'qtip-red qtip-shadow qtip-rounded' // Make it red... the classic error colour!
          }
        });
        $(target).css({
          'border-color': 'red'
        });
        $(target).on('click keypress', function () {
          $(this).css({
            'border-color': 'rgb(204, 204, 204)'
          });
        });
      };
      var error = false;
      // manage quote from and to selection
      var origin_selected = false;
      var destination_selected = false;

      // set marker when an option gets selected from the list
      $('.typeahead').on('typeahead:selected typeahead:autocompleted', function (e, suggestion) {
        if (e.target.id == 'quote_from') {
          origin_selected = true;
        }
        if (e.target.id == 'quote_to') {
          destination_selected = true;
        }
      });

      // reset marker when an option is written out by the user
      $('#quote_from').on('click', function (e) {
        if (e.target.id == 'quote_from') {
          origin_selected = false;
        }
      });
      $('#quote_to').on('click', function (e) {
        if (e.target.id == 'quote_to') {
          destination_selected = false;
        }
      });

      var COUNTRIES_WITHOUT_POSTCODES = [
        ["Angola", "AO"],
        ["Antigua and Barbuda", "AG"],
        ["Aruba", "AW"],
        ["Bahamas", "BS"],
        ["Belize", "BZ"],
        ["Benin", "BJ"],
        ["Botswana", "BW"],
        ["Burkina Faso", "BF"],
        ["Burundi", "BI"],
        ["Cameroon", "CM"],
        ["Central African Republic", "CF"],
        ["Comoros", "KM"],
        ["Congo", "CG"],
        ["Congo, the Democratic Republic of the", "CD"],
        ["Cook Islands", "CK"],
        ["Cote d'Ivoire", "CI"],
        ["Djibouti", "DJ"],
        ["Dominica", "DM"],
        ["Equatorial Guinea", "GQ"],
        ["Eritrea", "ER"],
        ["Fiji", "FJ"],
        ["French Southern Territories", "TF"],
        ["Gambia", "GM"],
        ["Ghana", "GH"],
        ["Grenada", "GD"],
        ["Guinea", "GN"],
        ["Guyana", "GY"],
        ["Hong Kong", "HK"],
        ["Jamaica", "JM"],
        ["Kenya", "KE"],
        ["Kiribati", "KI"],
        ["Macao", "MO"],
        ["Malawi", "MW"],
        ["Mali", "ML"],
        ["Mauritania", "MR"],
        ["Mauritius", "MU"],
        ["Montserrat", "MS"],
        ["Nauru", "NR"],
        ["Netherlands Antilles", "AN"],
        ["Niue", "NU"],
        ["North Korea", "KP"],
        ["Panama", "PA"],
        ["Qatar", "QA"],
        ["Rwanda", "RW"],
        ["Saint Kitts and Nevis", "KN"],
        ["Saint Lucia", "LC"],
        ["Sao Tome and Principe", "ST"],
        ["Saudi Arabia", "SA"],
        ["Seychelles", "SC"],
        ["Sierra Leone", "SL"],
        ["Solomon Islands", "SB"],
        ["Somalia", "SO"],
        ["Suriname", "SR"],
        ["Syria", "SY"],
        ["Tanzania, United Republic of", "TZ"],
        ["Timor-Leste", "TL"],
        ["Tokelau", "TK"],
        ["Tonga", "TO"],
        ["Tuvalu", "TV"],
        ["Uganda", "UG"],
        ["United Arab Emirates", "AE"],
        ["Vanuatu", "VU"],
        ["Yemen", "YE"],
        ["Zimbabwe", "ZW"],
        ["Chile", "CL"]
      ];

      $('#quote-form').on('submit', function (e) {
        error = false;
        var $weightValue = $('#quote-weight').val();
        // exception countries must simply not contain digits
        var exception_countries = ['cl', 'co'];

        var country_from = $(".flag-input-from").intlTelInput("getSelectedCountryData").iso2;
        var country_to = $(".flag-input-to").intlTelInput("getSelectedCountryData").iso2;

        $('#country_from').val(country_from);
        $('#country_to').val(country_to);

        if ($("#country_from").val() == "" || $("#country_from").val() == undefined) {
          error = true;
          showError($('#country_from'), "Please select origin country");
        }
        if ($("#country_to").val() == "" || $("#country_to").val() == undefined) {
          error = true;
          showError($('#country_to'), "Please select destination country");
        }

        if (exception_countries.indexOf(country_from) != -1) {
          if (/\d/.test($('#quote_from').val())) {
            error = true;
            showError($('#quote_from'), "La dirección de origen no debe contener dígitos para este país");
          }
        }
        // check origin typeahead input
        if (!origin_selected && (
            $('#quote_from').val().split(' ').length > 1 ||
            !/\d/.test($('#quote_from').val()))) {
          if ([].concat.apply([], COUNTRIES_WITHOUT_POSTCODES).includes(country_from.toUpperCase())) {
            showError($('#quote_from'),
              "Por favor ingresa un ciudad y elige la entrada correspondiente de la lista de resultados.");
          } else if (country_from.toUpperCase() == 'MX') {
            showError($('#quote_from'),
              "Por favor ingresa un código postal, ciudad o colonia y elige la entrada correspondiente de la lista de resultados."
              );
          } else {
            showError($('#quote_from'),
              "Por favor ingresa un código postal y elige la entrada correspondiente de la lista de resultados."
              );
          }
          // showError($('#quote_from'), "Por favor ingresa un código postal y elige la entrada correspondiente de la lista de resultados.");
          error = true;
          $('#quote-form').find(':submit').prop("disabled", false);
        }

        if (exception_countries.indexOf(country_to) != -1) {
          if (/\d/.test($('#quote_to').val())) {
            error = true;
            showError($('#quote_to'), "La dirección de destino no debe contener dígitos para este país");
          }
        }
        // check destination typeahead input
        if (!destination_selected && (
            $('#quote_to').val().split(' ').length > 1 ||
            !/\d/.test($('#quote_to').val()))) {
          if ([].concat.apply([], COUNTRIES_WITHOUT_POSTCODES).includes(country_to.toUpperCase())) {
            showError($('#quote_to'),
              "Por favor ingresa un ciudad y elige la entrada correspondiente de la lista de resultados.");
          } else if (country_to.toUpperCase() == 'MX') {
            showError($('#quote_to'),
              "Por favor ingresa un código postal, ciudad o colonia y elige la entrada correspondiente de la lista de resultados."
              );
          } else {
            showError($('#quote_to'),
              "Por favor ingresa un código postal y elige la entrada correspondiente de la lista de resultados."
              );
          }
          // showError($('#quote_to'), "Por favor ingresa un código postal, ciudad o colonia y elige la entrada correspondiente de la lista de resultados.");
          error = true;
          $('#quote-form').find(':submit').prop("disabled", false);
        }


        if ($('input[name="shipment_type"]:checked').length == 0) {
          showError(($('input[name="shipment_type"]')[0]),
            "Por favor selecciona el tipo de envío (Documento o Paquete).");
          error = true;
        }

        if ($('input[name="shipment_type"]:checked').val() == 1) {
          if ($weightValue <= 0) {
            showError($('#quote-weight'), "El peso del envío debe ser superior a 0.");
            error = true;
          }

          if ($.inArray(getCookie('locale'), ['en-US']) == 0 || $.inArray(getCookie('locale'), ['es-MX']) ==
            0) {
            if (!/^\d*(\.\d*)?$/.test($('#quote-weight').val())) {
              showError($('#quote-weight'),
                "El formato del peso es inválido. El separador decimal debe ser '.'");
              error = true;
            }
          } else {
            if (!/^\d*(\,\d*)?$/.test($('#quote-weight').val())) {
              showError($('#quote-weight'),
                "El formato del peso es inválido. El separador decimal debe ser ','");
              error = true;
            }
          }
        }

        if ($('input[name="shipment_type"]:checked').val() == 2) {
          if ($.inArray(getCookie('locale'), ['en-US']) == 0 || $.inArray(getCookie('locale'), ['es-MX']) ==
            0) {
            $.each($.find('.length'), function (index, element) {
              if (!/^\d*(\.\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del largo es inválido. El separador decimal debe ser '.'");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                showError(($(element)), "Length should be greater than zero");
                error = true;

              }
            });
            $.each($.find('.width'), function (index, element) {
              if (!/^\d*(\.\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del ancho es inválido. El separador decimal debe ser '.'");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                // showError(($(element)), "Width should be greater than zero");
                // error = true;
                console.log('error1', $(element).val());
              }
            });
            $.each($.find('.height'), function (index, element) {
              if (!/^\d*(\.\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del alto es inválido. El separador decimal debe ser '.'");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                showError(($(element)), "Height should be greater than zero");
                error = true;
              }
            });
            $.each($.find('.weight'), function (index, element) {
              if (!/^\d*(\.\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del peso es inválido. El separador decimal debe ser '.'");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                showError(($(element)), "Weight should be greater than zero");
                error = true;
              }
            });
          } else {
            $.each($.find('.length'), function (index, element) {
              if (!/^\d*(\,\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del largo es inválido. El separador decimal debe ser ','");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                showError(($(element)), "Length should be greater than zero");
                error = true;
              }
            });
            $.each($.find('.width'), function (index, element) {
              if (!/^\d*(\,\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del ancho es inválido. El separador decimal debe ser ','");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                showError(($(element)), "Width should be greater than zero");
                error = true;
              }
            });
            $.each($.find('.height'), function (index, element) {
              if (!/^\d*(\,\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del alto es inválido. El separador decimal debe ser ','");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                showError(($(element)), "Height should be greater than zero");
                error = true;
              }
            });
            $.each($.find('.weight'), function (index, element) {
              if (!/^\d*(\,\d*)?$/.test($(element).val())) {
                showError($(element),
                  "El formato del peso es inválido. El separador decimal debe ser ','");
                error = true;
              }
              if ($(element).val() == "" || $(element).val() < 0.01) {
                showError(($(element)), "Weight should be greater than zero");
                error = true;
              }
            });
          }
        }

        if (error) {
          e.preventDefault();
        } else if (/,/.test($('#quote-weight').val())) {
          $('#quote-weight').val($('#quote-weight').val().replace(',', '.'));
        }

      });

      jQuery('body').bind('click', function (e) {
        if (jQuery(e.target).closest('.navbar').length == 0) {
          // click happened outside of .navbar, so hide
          var opened = jQuery('.navbar-collapse').hasClass('collapse in');
          if (opened === true) {
            jQuery('.navbar-collapse').collapse('hide');
          }
        }
      });

      // Quote Weight Modal JS
      $('#quote-form').on('submit', function (e) {
        if (parseFloat($('form .js-weight').val()) > 70 && !error) {
          e.preventDefault();
          $('.modal .js-weight-value').text($('form .js-weight').val());
          $('.modal').modal('show');
        }
      });
      $('.modal .modal-footer').on('click', 'button', function () {
        var unit = $(this).data('unit');
        if (unit == 'g') {
          var new_weight = parseFloat($('form .js-weight').val()) / 1000;
          $('form .js-weight').val(new_weight);
        }
        $('#quote-form')[0].submit();
      });
    });
  });




  $(document).ready(function () {

    //CALL THE TOOLTIP
    $('[data-toggle="tooltip"]').tooltip();

    //$(".remove-volume-data span").tooltip();

    //CLONE OR REMOVE CLONE INSIDE FORM
    $("#paquete .volume-data").delegate(".clone-volume-data", "click", callClone);
    $("#paquete > .volume-data").delegate(".remove-volume-data", "click", removeClone);

    function getUrlVars() {
      var vars = {};
      var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
        function (m, key, value) {
          vars[key] = value;
        });
      return vars;
    }
    var getCookie = function (cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
      }
      return "";
    };

    if ((getUrlVars()["error_code"]) == "1") {
      $("#error_message").show();
      if ($.inArray(getCookie('locale'), ['en-US', 'en']) == 1) {
        $("#error_message").text("Please enter weight and dimensions for each parcel you are sending.")
      } else if ($.inArray(getCookie('locale'), ['es-CL', 'es-MX', 'es']) == 1) {
        $("#error_message").text("Por favor ingresa el peso y las dimensiones del paquete que estas enviando.")
      } else if ($.inArray(getCookie('locale'), ['de-De', 'de']) == 1) {
        $("#error_message").text(
          "Bitte geben Sie das Gewicht und die Dimensionen des Paketes, dass Sie senden möchten, ein.")
      } else {
        $("#error_message").text("Please enter weight and dimensions for each parcel you are sending.")
      }
    }

    if ((getUrlVars()["error_code"]) == "2") {
      $("#error_message").show();
      if ($.inArray(getCookie('locale'), ['en-US', 'en']) == 1) {
        $("#error_message").text(
          "We are sorry. You tried to modify or continue a shipment that cannot be modified anymore. Please start a new one."
          )
      } else if ($.inArray(getCookie('locale'), ['es-CL', 'es-MX', 'es']) == 1) {
        $("#error_message").text(
          "Lo sentimos. Intentaste modificar o continuar un envío que ya no puede ser modificado. Por favor genera un envío nuevo."
          )
      } else if ($.inArray(getCookie('locale'), ['de-De', 'de']) == 1) {
        $("#error_message").text(
          "Ups, Sie haben versucht eine  Sendung zu bearbeiten oder fortsetzen, die nicht mehr fortgesetzt werden kann. Bitte eretellen Sie eine neue Sendung."
          )
      } else {
        $("#error_message").text(
          "We are sorry. You tried to modify or continue a shipment that cannot be modified anymore. Please start a new one."
          )
      }
    }

    function callClone() {
      event.preventDefault();

      $(".clone-volume-data span").tooltip('destroy');

      var templement = $("#paquete .volume-data").first().clone(true, true);
      var index = parseInt($('.volume-data').length);
      templement.attr('id', index);
      templement.find('.quantity').attr('name', 'shipment[parcels][' + index + '][quantity]');
      templement.find('.length').attr('name', 'shipment[parcels][' + index + '][length]');
      templement.find('.width').attr('name', 'shipment[parcels][' + index + '][width]');
      templement.find('.height').attr('name', 'shipment[parcels][' + index + '][height]');
      templement.find('.weight').attr('name', 'shipment[parcels][' + index + '][weight]');
      templement.find('.length').val('');
      templement.find('.width').val('');
      templement.find('.height').val('');
      templement.find('.weight').val('');
      templement.find('.remove-volume-data').attr('id', 'remove-' + index + '');
      templement.css("margin-top", "10px");

      templement.appendTo($(".placement-indicator"));

      var elToShow = templement.find(".remove-volume-data");
      elToShow.css("display", "inline-block");

      $(".alert-info-box").css("display", "inline-block");

    };

    function removeClone() {

      event.preventDefault();

      //CHECK IN CASE IF WE HAVE MORE THAN ONE ELEMENT

      var piecesAmount = document.getElementsByClassName("clone-volume-data");

      if (piecesAmount.length > 1) {
        $(this).parent().parent().remove();
      }
      piecesAmount = document.getElementsByClassName("clone-volume-data");
      if (piecesAmount.length == 1) {
        $(".alert-info-box").css("display", "none");
      }

    };

    //TABS INSIDE FORM
    $('input[name="shipment_type"]').click(function () {
      $(this).parent().addClass('activeInput');
      let need = $(this).attr('data-target');
      $('#documento, #paquete').removeClass('showTab');
      $(need).addClass('showTab');
    });

    //EXPLANATION BLOCK ALERT
    $('.alert-close').click(function () {
      $(this).parent().css("display", "none");
    });

    //ADDITIONAL OPTIONS AT THE END OF THE FORM
    $(".checked-param-option").click(function () {
      if ($('.checked-param-option').is(':checked')) {
        $('.checked-param').css("display", "inline-flex");
        $('.checked-param').css("vertical-align", "middle");
      } else {
        $('.checked-param').css("display", "none");
      }
    });

    //CHANGING THE UNIT
    $('.link-selection').click(function () {
      event.preventDefault();

      var inactiveElement = $(this).parent().find('.inactive');
      var currentElement = $(this);

      var curValue;

      if (currentElement.hasClass('lbs')) {
        curValue = "kg";

      } else {
        curValue = "lbs";
      }
      $('#parcel-weight-unit').val(curValue);
      inactiveElement.removeClass('inactive');
      inactiveElement.addClass('current');

      currentElement.removeClass('current');
      currentElement.addClass('inactive');

      //CHANGING values inside fields
      var test = document.getElementsByClassName("field-value");

      for (i = 0; i < test.length; i++) {

        switch (curValue) {
          case "lbs":
            if (test[i].innerHTML == "cm") {
              $(test[i]).text('in');
              $(test[i]).parent().find('input').attr("placeholder", "in");
            }
            if (test[i].innerHTML == "kg") {
              $(test[i]).text('lbs');
              $(test[i]).parent().find('input').attr("placeholder", "lbs");
            }

            break;

          case "kg":
            if (test[i].innerHTML == "in") {
              $(test[i]).text('cm');
              $(test[i]).parent().find('input').attr("placeholder", "cm");
            }
            if (test[i].innerHTML == "lbs") {
              $(test[i]).text('kg');
              $(test[i]).parent().find('input').attr("placeholder", "kg");
            }

            break;
        }

      }
    });
  });
