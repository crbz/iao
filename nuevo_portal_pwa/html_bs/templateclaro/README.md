** Edite un archivo, cree un nuevo archivo y clone desde Bitbucket en menos de 2 minutos **

Cuando haya terminado, puede eliminar el contenido de este archivo README y actualizar el archivo con detalles para que otros comiencen a usar su repositorio.

* Le recomendamos que abra este archivo README en otra pestaña mientras realiza las tareas a continuación. Puede [ver nuestro video] (https://youtu.be/0ocf7u76WSo) para obtener una demostración completa de todos los pasos de este tutorial. Abra el video en una nueva pestaña para evitar salir de Bitbucket. *

---

## Editar un archivo

Comenzará editando este archivo README para aprender a editar un archivo en Bitbucket.

1. Haga clic en ** Fuente ** en el lado izquierdo.
2. Haga clic en el enlace README.md de la lista de archivos.
3. Haga clic en el botón ** Editar **.
4. Elimine el siguiente texto: * Elimine esta línea para realizar un cambio en el archivo README de Bitbucket. *
5. Después de realizar su cambio, haga clic en ** Confirmar ** y luego ** Confirmar ** nuevamente en el cuadro de diálogo. La página de confirmación se abrirá y verá el cambio que acaba de hacer.
6. Regrese a la página ** Fuente **.

---

## Crear un archivo

A continuación, agregará un nuevo archivo a este repositorio.

1. Haga clic en el botón ** Nuevo archivo ** en la parte superior de la página ** Fuente **.
2. Dé al archivo un nombre de archivo de ** contributors.txt **.
3. Ingrese su nombre en el espacio de archivo vacío.
4. Haga clic en ** Confirmar ** y luego en ** Confirmar ** nuevamente en el cuadro de diálogo.
5. Regrese a la página ** Fuente **.

Antes de continuar, continúe y explore el repositorio. Ya has visto la página ** Fuente **, pero mira las páginas ** Compromisos **, ** Ramas ** y ** Configuración **.

---

## Clonar un repositorio

Use estos pasos para clonar desde SourceTree, nuestro cliente para usar la línea de comandos del repositorio gratis. La clonación le permite trabajar en sus archivos localmente. Si aún no tiene SourceTree, [descargue e instale primero] (https://www.sourcetreeapp.com/). Si prefiere clonar desde la línea de comandos, consulte [Clonar un repositorio] (https://confluence.atlassian.com/x/4whODQ).

1. Verá el botón de clonar debajo del encabezado ** Fuente **. Haz clic en ese botón.
2. Ahora haga clic en ** Echa un vistazo en SourceTree **. Es posible que deba crear una cuenta de SourceTree o iniciar sesión.
3. Cuando vea el cuadro de diálogo ** Clonar nuevo ** en SourceTree, actualice la ruta y el nombre de destino si lo desea y luego haga clic en ** Clonar **.
4. Abra el directorio que acaba de crear para ver los archivos de su repositorio.

Ahora que está más familiarizado con su repositorio de Bitbucket, continúe y agregue un nuevo archivo localmente. Puede [enviar su cambio a Bitbucket con SourceTree] (https://confluence.atlassian.com/x/iqyBMg), o puede [agregar, confirmar,] (https://confluence.atlassian.com/x/ 8QhODQ) y [presionar desde la línea de comando] (https://confluence.atlassian.com/x/NQ0zDQ).