const URL = 'https://pokeapi.co/api/v2/pokemon/?limit=151';

const urlOficialimages= 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png';
  //https://pokeapi.co/api/v2/generation/1
  const container = document.getElementById('container');
  const otherImages='https://raw.githubusercontent.com/anchetaWern/pokeapi-json/master/data/v1/media/img/1.png';
//const imagesPOkemonGo= 'https://www.serebii.net/pokemongo/pokemon/113.png';
  const URL2 = 'https://pokeapi.co/api/v2/generation/1/';
  //pokemon_species[name,url];
  //var selection = document.querySelector('input[name="generation"]:checked').value;
  var numero=1;
  //var btnClick = document.getElementById('btnClick');
  getPokemons(numero);
  btnFirstGen.style.display="none";

  btnFirstGen.addEventListener('click', function(){
    numero = 1;
    //console.log('1st: ' + numero);
     toggle=!toggle;
    getPokemons(numero,false);
    
  });
  var toggle=false;
  btnAllSchool.addEventListener('click',function(){
    toggle=!toggle;
    
    //console.error("click");
    getPokemons(numero,toggle);
    
  });
  var geners = ['generation-1', 'generation-2', 'generation-3', 'generation-4',
                'generation-5', 'generation-6', 'generation-7']
  
  var filters = document.getElementById('filters');
  var gen = '';
  for(var i = 0; i<geners.length;i++){
    gen += `
      <input class="radio-gens" type="radio" id=${geners[i]} value=${i+1} name="generation" checked>
      <label for=${geners[i]} class="label-gens">${geners[i]} </label>`;
  }
  filters.innerHTML=gen;
  
  filters.addEventListener('click', function(e){
    var targ = e.target.type;
    if(targ=="radio") {
      //console.log("value:" + e.target.id)
      getPokemons(e.target.value,toggle);
      title.innerHTML='Pokemon '+ e.target.id;
    }
    else{
      console.log('not radio');
    }
  });
    

function imgLoaded(img){
    var imagelink = img.parentNode;
 
    imagelink.className += imagelink.className ? ' loaded' : 'loaded';
};

function orderNumber(str){
  var mySubString = str.substring(
    str.lastIndexOf("s/") + 2, 
    str.lastIndexOf("/")
  );
  return mySubString;
}
//https://vignette.wikia.nocookie.net/es.pokemon/images/4/43/Bulbasaur.png
let html=''; 
function getPokemons(numero,toggle) {
  
  let urlLo = `https://pokeapi.co/api/v2/generation/${numero}/`;
  console.log("url: "+ urlLo);
   var container = document.getElementById('container');
  container.innerHTML='';
   let pokemons=[];
  fetch(urlLo, {mode: 'cors'})
    .then(resp => resp.json())
    .then(data => {
     
      pokemons = data.pokemon_species;
      
      for(let j=0;j<pokemons.length;j++){
        pokemons[j].nr=orderNumber(pokemons[j].url);
      }
      pokemons.sort((a,b)=>(a.nr)-(b.nr));
      //console.log(pokemons);
     var html='';
      var numero3decimals='';
      pokemons.map((pokemon) => {
        var numero3decimals = orderNumber(pokemon.url);
        if(numero3decimals<10){
          numero3decimals='0'+numero3decimals;
        }
        if(numero3decimals<100){
          numero3decimals='0'+numero3decimals;
        }
      
        
        var toggleurl =toggle ? 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/' :'https://www.serebii.net/pokemongo/pokemon/';
      html+=`<li class="item">
              <div> ${orderNumber(pokemon.url)} - ${pokemon.name}</div>
              <img class="pokeimage" src='${toggleurl}${numero3decimals}.png'>
          </li>`;
        

        //listener(pokemon.name,index); 
      });
      container.innerHTML+=html; 
    });
 
} 
//https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png
//https://www.serebii.net/pokemongo/pokemon/${numero3decimals}.png


