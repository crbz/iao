const navSlide = () => {
  const hmenu = document.querySelector(".hmenu");
  const nav = document.querySelector(".nav-links");
  const navlinks = document.querySelectorAll(".nav-links li");
 
  hmenu.addEventListener("click", () => {
    //actviar boton de menu
    nav.classList.toggle("nav-active");

    //animation
    navlinks.forEach((links, index) => {
      if (links.style.animation) {
        links.style.animation = "";
      } else {
        links.style.animation = `linkMenu 0.5s ease forwards ${index / 6 
        }s`;
     
      }
    });

    //animacion de boton
    hmenu.classList.toggle("toogle");
  });
  //
};


const btnSwitch = document.querySelector("#switch");

btnSwitch.addEventListener("click", () => {
     document.body.classList.toggle("darkmode");
     btnSwitch.classList.toggle("active")

});

// iniciar aplicaciones
const app= ()=>{
    navSlide();
  
    
};



app();